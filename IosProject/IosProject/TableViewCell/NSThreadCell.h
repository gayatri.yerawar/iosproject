//
//  NSThreadCell.h
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSThreadCell : UITableViewCell

@property (strong,nonatomic) UILabel *login;
@property (strong,nonatomic) UILabel *Id;
@property (strong, nonatomic) UIImageView *imgview;

@end
