//
//  NSOperationCell.m
//  IosProject
//
//  Created by Mac on 12/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NSOperationCell.h"

@implementation NSOperationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //Image
        self.imgview = [[UIImageView alloc]initWithFrame:CGRectMake(20,10, 130, 130)];
        [_imgview setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:self.imgview];
        
        //name
        self.lbrand = [[UILabel alloc] init];
        self.lbrand.textColor = [UIColor blackColor];
        self.lbrand.textAlignment = NSTextAlignmentCenter;
        self.lbrand.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:228.0/255.0 blue:238.0/255.0 alpha:1];
        self.lbrand.font = [UIFont fontWithName:@"Arial" size:19.0f];
        [_lbrand setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:self.lbrand];
        
        NSLayoutConstraint *leftname = [NSLayoutConstraint constraintWithItem:_lbrand attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:170];
        NSLayoutConstraint *topname = [NSLayoutConstraint constraintWithItem:_lbrand attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:40];
        
        NSLayoutConstraint *heightname = [NSLayoutConstraint constraintWithItem:_lbrand attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:25];
        NSLayoutConstraint *rightname = [NSLayoutConstraint constraintWithItem:_lbrand attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-20];
        
        [self addConstraints:@[leftname, topname, heightname, rightname]];
        
        //capital
        self.lproduct_type = [[UILabel alloc] init];
        self.lproduct_type.textColor = [UIColor blackColor];
        self.lproduct_type.textAlignment = NSTextAlignmentCenter;
        self.lproduct_type.numberOfLines = 0;
        self.lproduct_type.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:228.0/255.0 blue:238.0/255.0 alpha:1];
        self.lproduct_type.font = [UIFont fontWithName:@"Arial" size:19.0f];
        [_lproduct_type setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:self.lproduct_type];
        
        NSLayoutConstraint *leftcapital = [NSLayoutConstraint constraintWithItem:_lproduct_type attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:170];
        NSLayoutConstraint *topcapital = [NSLayoutConstraint constraintWithItem:_lproduct_type attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:80];
        
        NSLayoutConstraint *heightcapital = [NSLayoutConstraint constraintWithItem:_lproduct_type attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:25];
        NSLayoutConstraint *rightcapital = [NSLayoutConstraint constraintWithItem:_lproduct_type attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-20];
        
        [self addConstraints:@[leftcapital, topcapital, heightcapital, rightcapital]];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
