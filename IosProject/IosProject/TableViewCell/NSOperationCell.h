//
//  NSOperationCell.h
//  IosProject
//
//  Created by Mac on 12/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSOperationCell : UITableViewCell

@property (strong,nonatomic) UILabel *lbrand;
@property (strong,nonatomic) UILabel *lproduct_type;
@property (strong, nonatomic) UIImageView *imgview;

@end
