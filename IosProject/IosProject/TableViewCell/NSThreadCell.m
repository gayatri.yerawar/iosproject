//
//  NSThreadCell.m
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NSThreadCell.h"

@implementation NSThreadCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //Image
        self.imgview = [[UIImageView alloc]initWithFrame:CGRectMake(20,10, 130, 130)];
        [_imgview setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:self.imgview];
        
        //login
        self.login = [[UILabel alloc] init];
        self.login.textColor = [UIColor blackColor];
        self.login.textAlignment = NSTextAlignmentCenter;
        self.login.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:228.0/255.0 blue:238.0/255.0 alpha:1];
        self.login.font = [UIFont fontWithName:@"Arial" size:19.0f];
        [_login setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:self.login];
        
        NSLayoutConstraint *leftlogin = [NSLayoutConstraint constraintWithItem:_login attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:170];
        NSLayoutConstraint *toplogin = [NSLayoutConstraint constraintWithItem:_login attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:40];
        
        NSLayoutConstraint *heightlogin = [NSLayoutConstraint constraintWithItem:_login attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:25];
        NSLayoutConstraint *rightlogin = [NSLayoutConstraint constraintWithItem:_login attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-20];
        
        [self addConstraints:@[leftlogin, toplogin, heightlogin, rightlogin]];
        
        //Id
//        self.Id = [[UILabel alloc] init];
//        self.Id.textColor = [UIColor blackColor];
//        self.Id.textAlignment = NSTextAlignmentCenter;
//        self.Id.numberOfLines = 0;
//        self.Id.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:228.0/255.0 blue:238.0/255.0 alpha:1];
//        self.Id.font = [UIFont fontWithName:@"Arial" size:19.0f];
//        [_Id setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [self addSubview:self.Id];
//
//        NSLayoutConstraint *leftId = [NSLayoutConstraint constraintWithItem:_Id attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:170];
//        NSLayoutConstraint *topId = [NSLayoutConstraint constraintWithItem:_Id attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:80];
//
//        NSLayoutConstraint *heightId = [NSLayoutConstraint constraintWithItem:_Id attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:25];
//        NSLayoutConstraint *rightId = [NSLayoutConstraint constraintWithItem:_Id attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-20];
//
//        [self addConstraints:@[leftId, topId, heightId,rightId]];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
