//
//  LogoDatabaseManager.h
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LogoDatabaseManager : NSObject

+ (id)sharedInstancelogo;

-(NSArray *) getLogoDB;

-(void) saveData:(NSDictionary *) object;
-(BOOL) isLogoPresentInDB: (NSDictionary *) duplicate;

@end

