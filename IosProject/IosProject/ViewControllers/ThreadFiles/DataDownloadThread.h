//
//  DataDownloadThread.h
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataDownloadThread : NSThread

@property(nonatomic, strong) NSURL *url;
@property (nonatomic, strong) void (^DataCallback)(NSData *data, NSError *error);

- (id)initWithURL: (NSURL *)url andCallBack: (void (^)( NSData *data, NSError *error))completionHandler;

@end
