//
//  DataDownloadThread.m
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "DataDownloadThread.h"

@implementation DataDownloadThread

-(void)main{
    [super main];
    NSData *data = [NSData dataWithContentsOfURL:self.url];
    self.DataCallback(data, nil);
    
}

- (id)initWithURL: (NSURL *)url andCallBack: (void (^)( NSData *data, NSError *error))completionHandler{
    
    self = [super init];
    self.url = url;
    self.DataCallback = completionHandler;
    
    return self;
    
}

@end
