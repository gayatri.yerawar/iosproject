//
//  ImageDownloadThread.h
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageDownloadThread : NSThread

@property(nonatomic, strong) NSURL *url;
@property (nonatomic, strong) void (^ImageCallback)(NSString *login, UIImage *img, NSError *error);
@property (nonatomic,strong) NSString *login;

- (id)initWithlogo:(NSString *)login andWithURL: (NSURL *)url andCallBack: (void (^)(NSString *login, UIImage *img, NSError *error))completionHandler;

@end
