//
//  NSThreadVC.m
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NSThreadVC.h"
#import "NSThreadCell.h"
#import "ThreadModel.h"
#import "DataDownloadThread.h"
#import "ImageDownloadThread.h"

@interface NSThreadVC ()

@property (nonatomic,strong) NSMutableArray<ThreadModel *> *arr;

@end

@implementation NSThreadVC
{
    UITableView *tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self tabledata];
    [self NSThreadData];
   
    self.arr = [[NSMutableArray alloc]init];
}

-(void)tabledata{
    
    tableView = [[UITableView alloc]init];
    [tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    tableView.allowsSelection = UITableViewCellSelectionStyleNone;
    [self.view addSubview:tableView];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:-0];
    
    [self.view addConstraints:@[left, top, bottom, right]];
    
    [tableView registerClass:[NSThreadCell class] forCellReuseIdentifier:@"Cell"];
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.dataSource = self;
    tableView.delegate = self;
}

-(void)NSThreadData{
    NSURL *url = [NSURL URLWithString:@"https://api.github.com/users/hadley/orgs"];

     void(^DataDownloadCallBack)(NSData *data, NSError *error) = ^(NSData *Data, NSError *error){
          NSLog(@"Data: %@",Data);
         if(Data != nil){
             NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:Data options:NSJSONReadingAllowFragments error:nil];
             NSLog(@"The response is - %@",responseArray);

             _arr = [ThreadModel modelArrayFromArr:responseArray];
             
             for(int i=0;i<_arr.count;i++){
                 
                 void(^ImageDownloadCallBack)(NSString *login, UIImage *path, NSError *error) = ^(NSString *login, UIImage *image, NSError *error){
                     
                     for(int j=0;j<_arr.count;j++){
                         
                         ThreadModel *logoimage = [_arr objectAtIndex:i];
                         
                         if([logoimage.login_name isEqualToString:login]){
                             
                             logoimage.image = image;
                         }
                     }
                 };
                 
                 ThreadModel *logo = [_arr objectAtIndex:i];
                 NSURL *u = [NSURL URLWithString:logo.imgUrl];
                 ImageDownloadThread *imagedownload = [[ImageDownloadThread alloc]initWithlogo:logo.login_name andWithURL:u andCallBack:ImageDownloadCallBack];
                 [imagedownload start];
             }

             dispatch_async(dispatch_get_main_queue(), ^{
                 [self->tableView reloadData];
             });
         }

     };
    DataDownloadThread * datadownload = [[DataDownloadThread alloc]initWithURL:url andCallBack:DataDownloadCallBack];
    [datadownload start];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    NSThreadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil){
        
        cell = [[NSThreadCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    ThreadModel *display = _arr[indexPath.row];
    cell.login.text = [display login_name];
//    cell.Id.tag = [display Lid];
    cell.imgview.image = [display image];
    
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arr.count;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 170;
}

@end
