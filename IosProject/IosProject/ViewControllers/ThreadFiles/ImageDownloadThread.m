//
//  ImageDownloadThread.m
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ImageDownloadThread.h"

@implementation ImageDownloadThread

-(void)main{
    [super main];
    
    NSString *dgbreed = [NSString stringWithString:self.login];
    UIImage *imgdata = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.url]];
    self.ImageCallback(dgbreed, imgdata, nil);
    
}

- (id)initWithlogo:(NSString *)login andWithURL: (NSURL *)url andCallBack: (void (^)(NSString *login, UIImage *img, NSError *error))completionHandler{
    
    self = [super init];
    self.login = login;
    self.url = url;
    self.ImageCallback = completionHandler;
    
    return self;
}

@end
