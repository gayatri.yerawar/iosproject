//
//  NSOperationVC.m
//  IosProject
//
//  Created by Mac on 12/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "NSOperationVC.h"
#import "NSOperationCell.h"
#import "OperationModel.h"
#import "JSONDownloadOperation.h"
#import "ImageDownloadOperation.h"
#import "MakeupDatabaseManager.h"
#import "Makeup+CoreDataClass.h"
#import "Makeup+CoreDataProperties.h"

@interface NSOperationVC ()

@property (nonatomic,strong) NSMutableArray< Makeup *> *arrMakeup;
@property (nonatomic,strong) NSMutableArray< OperationModel *> *arrmodel;

@end

@implementation NSOperationVC

{
    UITableView *tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self tabledata];
    [self NSOperationData];
    
    self.arrMakeup = [[NSMutableArray alloc]init];
    self.arrmodel = [[NSMutableArray alloc]init];

}

-(void)tabledata{
    
    tableView = [[UITableView alloc]init];
    [tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    tableView.allowsSelection = UITableViewCellSelectionStyleNone;
    [self.view addSubview:tableView];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:-0];
    
    [self.view addConstraints:@[left, top, bottom, right]];
    
    [tableView registerClass:[NSOperationCell class] forCellReuseIdentifier:@"Cell"];
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.dataSource = self;
    tableView.delegate = self;
}

-(void)NSOperationData{
    
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc]init];
    NSURL *url1 = [NSURL URLWithString:@"http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline"];

    void(^DataDownloadCallBack)(NSData *data, NSError *error) = ^(NSData *data, NSError *error){
        NSLog(@"Data: %@", data);
        if(data != nil){
            
            NSDictionary *responsedict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"The response is - %@",responsedict);
            
       //     _arrmodel = [OperationModel modelArrayFromDict:responsedict];
            
            for(int i=0; i<_arrmodel.count; i++){
                
                void(^ImageDownloadCallBack)(NSString *product_type, UIImage *path, NSError *error) = ^(NSString *product_type, UIImage *path, NSError *error){

                    for(int j=0; j<_arrmodel.count; j++){

                        OperationModel *Image = [_arrmodel objectAtIndex:i];

                    if([Image.product_type isEqualToString:product_type]){
                        Image.img = path;
                      }
                    }
                };
               
                OperationModel *mkeup = [_arrmodel objectAtIndex:i];
                NSURL *u = [NSURL URLWithString:mkeup.url];
                ImageDownloadOperation *imagedown = [[ImageDownloadOperation alloc]initWithname:mkeup.brand andWithURL:u andCallBack:ImageDownloadCallBack];
                [operationQueue addOperation:imagedown];
            }
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                for (NSDictionary *arr in responsedict){
                    
                    BOOL isPresent = [[MakeupDatabaseManager sharedInstance] isProductPresentInDB:arr];
                    
                    if (isPresent == NO) {
                        
                    [[MakeupDatabaseManager sharedInstance] saveData:arr];

                    }
                }
                
         _arrMakeup = [NSMutableArray arrayWithArray:[[MakeupDatabaseManager sharedInstance]getMakeupDB]];
                [self->tableView reloadData];
            }];
        }
    };
    
    JSONDownloadOperation *displaydata = [[JSONDownloadOperation alloc]initWithURL: url1 andCallBack: DataDownloadCallBack];
    
    [operationQueue addOperation: displaydata];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    NSOperationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil){
        
        cell = [[NSOperationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    if ([self.arrMakeup count]>0)
       {
           NSLog(@"brand == %@",[self.arrMakeup lastObject]);
           Makeup *data = [self.arrMakeup objectAtIndex:indexPath.row];
            cell.lbrand.text = [data pbrand];
            cell.lproduct_type.text = [data productType];
        // cell.imgview.image = [data img];

       }
  //  OperationModel *data = _arrmodel[indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arrMakeup.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
