//
//  MakeupDatabaseManager.m
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "MakeupDatabaseManager.h"
#import "Makeup+CoreDataClass.h"
#import "Makeup+CoreDataProperties.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "OperationModel.h"



@implementation MakeupDatabaseManager

+ (id)sharedInstance {
    static MakeupDatabaseManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (AppDelegate *)appDelegate{
    return (AppDelegate *)[[UIApplication sharedApplication]delegate];
}

- (NSManagedObjectContext *)managedObjectContext{
    return [[self appDelegate] persistentContainer].viewContext;
}

-(BOOL) isProductPresentInDB: (NSDictionary *) duplicate{
    
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Makeup"];
      NSString *strptype = [duplicate valueForKey:@"product_type"];
      request.predicate = [NSPredicate predicateWithFormat:@"productType = %@",strptype];
      NSArray *result  = [self.managedObjectContext executeFetchRequest: request error:nil];
    
      if(result.count >0){
          return YES;
      }else{
          return NO;
      }
}

-(NSArray *) getMakeupDB {

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Makeup"];
    NSArray *result  = [self.managedObjectContext executeFetchRequest: request error:nil];
    return result;
}

-(void) saveData:(NSDictionary *) object{
        
    Makeup *makeup = [NSEntityDescription insertNewObjectForEntityForName: @"Makeup" inManagedObjectContext: [self managedObjectContext]];

    makeup.pbrand  = [object valueForKey:@"brand"];
    makeup.productType = [object valueForKey:@"product_type"];


        NSError *error = nil;

        if ([[self managedObjectContext] save:&error] == NO)
        {
        NSAssert(NO, @"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
        }

   }



@end
