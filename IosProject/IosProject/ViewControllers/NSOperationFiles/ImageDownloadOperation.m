//
//  ImageDownloadOperation.m
//  Json Parsing 2
//
//  Created by Mac on 21/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ImageDownloadOperation.h"
#import <UIKit/UIKit.h>
#import "NSOperationVC.h"
#import "OperationModel.h"


@implementation ImageDownloadOperation

-(void) main{
    [super main];
    
    NSString *cityname = [NSString stringWithString:self.name];
   UIImage *imgdata = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.url]];
   self.ImageCallback(cityname, imgdata, nil);
}

- (id)initWithname:(NSString *)name andWithURL: (NSURL *)url andCallBack: (void (^)(NSString *name, UIImage *img, NSError *error))completionHandler{
    
    self = [super init];
    self.name = name;
    self.url = url;
    self.ImageCallback = completionHandler;
    return self;
}

@end

