//
//  MakeupDatabaseManager.h
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Makeup+CoreDataClass.h"
#import "Makeup+CoreDataProperties.h"
#import <CoreData/CoreData.h>
#import "OperationModel.h"



@interface MakeupDatabaseManager : NSObject

@property (nonatomic,strong) AppDelegate *appDelegate;
@property( strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic, readonly) NSPersistentContainer *persistentContainer;

+ (id)sharedInstance;

-(NSArray *) getMakeupDB;

-(void) saveData:(NSDictionary *) object;
-(BOOL) isProductPresentInDB: (NSDictionary *) duplicate;

@end

