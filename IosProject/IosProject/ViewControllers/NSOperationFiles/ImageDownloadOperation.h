//
//  ImageDownloadOperation.h
//  Json Parsing 2
//
//  Created by Mac on 21/09/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ImageDownloadOperation : NSOperation

@property(nonatomic, strong) NSURL *url;

@property (nonatomic, strong) void (^ImageCallback)(NSString *name, UIImage *img, NSError *error);
@property (nonatomic,strong) NSString *name;

- (id)initWithname:(NSString *)name andWithURL: (NSURL *)url andCallBack: (void (^)(NSString *name, UIImage *img, NSError *error))completionHandler;



@end


