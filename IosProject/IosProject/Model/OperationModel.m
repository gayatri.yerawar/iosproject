//
//  OperationModel.m
//  IosProject
//
//  Created by Mac on 12/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "OperationModel.h"

@implementation OperationModel

+(NSMutableArray < OperationModel *> *)modelArrayFromDict:(NSDictionary *) d
{
    NSMutableArray *dogarr = [[NSMutableArray alloc]init];
    
    if (d != nil){
        
        for(NSArray *arr in d){
            
            OperationModel *Details = [[OperationModel alloc]init];
            
            NSString *strbrand = [arr valueForKey:@"brand"];
            Details.brand = strbrand;
            
            NSString *strProducttype = [arr valueForKey:@"product_type"];
            Details.product_type = strProducttype;
            
            NSString *strurl = [arr valueForKey: @"image_link"];
            Details.url = strurl;
            
            [dogarr addObject:Details];
        }
    }
    return dogarr;
}

@end
