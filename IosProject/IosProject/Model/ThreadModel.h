//
//  ThreadModel.h
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ThreadModel : NSObject

@property (strong,nonatomic) NSString *login_name;
//@property (assign,nonatomic) NSInteger Lid;
//@property (strong,nonatomic) NSString *logoid;
@property (strong,nonatomic) UIImage *image;
@property (strong,nonatomic) NSString *imgUrl;

+(NSMutableArray < ThreadModel * > *)modelArrayFromArr:(NSArray *) array;

@end
