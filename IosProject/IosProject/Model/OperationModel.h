//
//  OperationModel.h
//  IosProject
//
//  Created by Mac on 12/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OperationModel : NSObject

@property (strong,nonatomic) NSString *brand;
@property (strong,nonatomic) NSString *product_type;
@property (strong,nonatomic) UIImage  *img;
@property (strong,nonatomic) NSString  *url;

+(NSMutableArray < OperationModel * > *)modelArrayFromDict:(NSDictionary *) d;

@end
