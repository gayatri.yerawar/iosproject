//
//  ThreadModel.m
//  IosProject
//
//  Created by Mac on 08/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

#import "ThreadModel.h"
#import "NSThreadVC.h"

@implementation ThreadModel

+(NSMutableArray < ThreadModel * > *)modelArrayFromArr:(NSArray *) array{
    
    NSMutableArray *dataArr = [[NSMutableArray alloc]init];
    
    if(array != nil){
        
        for(NSDictionary *dict in array){
            
            ThreadModel *details = [[ThreadModel alloc]init];
            
            NSString *strlogin = [dict objectForKey:@"login"];
            details.login_name = strlogin;
            
//            NSInteger intid = [[dict objectForKey:@"id"]integerValue];
//            details.Lid = intid;
            
            NSString *strurl = [dict objectForKey:@"avatar_url"];
            details.imgUrl = strurl;
            
            [dataArr addObject:details];
        }
    }
    
    return dataArr;
}

@end
