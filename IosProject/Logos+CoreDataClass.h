//
//  Logos+CoreDataClass.h
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Logos : NSManagedObject

@end

#import "Logos+CoreDataProperties.h"
