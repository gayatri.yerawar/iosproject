//
//  Logos+CoreDataProperties.m
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//
//

#import "Logos+CoreDataProperties.h"

@implementation Logos (CoreDataProperties)

+ (NSFetchRequest<Logos *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Logos"];
}

@dynamic loginName;

@end
