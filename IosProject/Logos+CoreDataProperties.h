//
//  Logos+CoreDataProperties.h
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//
//

#import "Logos+CoreDataClass.h"


@interface Logos (CoreDataProperties)

+ (NSFetchRequest<Logos *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *loginName;

@end

