//
//  Makeup+CoreDataProperties.h
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//
//

#import "Makeup+CoreDataClass.h"


@interface Makeup (CoreDataProperties)

+ (NSFetchRequest<Makeup *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *pbrand;
@property (nullable, nonatomic, copy) NSString *productType;

@end
