//
//  Makeup+CoreDataProperties.m
//  IosProject
//
//  Created by Mac on 19/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//
//

#import "Makeup+CoreDataProperties.h"

@implementation Makeup (CoreDataProperties)

+ (NSFetchRequest<Makeup *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Makeup"];
}

@dynamic pbrand;
@dynamic productType;

@end
